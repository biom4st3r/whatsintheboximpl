package com.biom4st3r.witb.mxn;

import com.biom4st3r.witb.interfaces.ChestBEAccess;

import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.client.block.ChestAnimationProgress;
import net.minecraft.util.Identifier;
import net.minecraft.util.Tickable;

@Mixin(ChestBlockEntity.class)
public abstract class ChestBEMxn extends LootableContainerBlockEntity implements ChestAnimationProgress, Tickable, ChestBEAccess
{

	protected ChestBEMxn(BlockEntityType<?> blockEntityType_1) {
		super(blockEntityType_1);
	}

    @Override
    public Identifier getLootTableID() {
        return this.lootTableId;
    }
    


}