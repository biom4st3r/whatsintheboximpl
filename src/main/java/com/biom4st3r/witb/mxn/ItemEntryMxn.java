package com.biom4st3r.witb.mxn;

import com.biom4st3r.witb.interfaces.ItemEntryAccess;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import net.minecraft.item.Item;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.function.LootFunction;

@Mixin(ItemEntry.class)
public abstract class ItemEntryMxn extends LeafEntry implements ItemEntryAccess {

    protected ItemEntryMxn(int int_1, int int_2, LootCondition[] lootConditions_1, LootFunction[] lootFunctions_1) {
        super(int_1, int_2, lootConditions_1, lootFunctions_1);
    }

    @Shadow
    @Final
    private Item item;
    // @Shadow
    // @Final
    // protected int weight;
    // @Shadow
    // @Final
    // protected int quality;
    // @Shadow
    // @Final
    // protected LootFunction[] functions;

    @Override
    public Item getItem() {
        
        return item;
    }

    // @Override
    // public LootFunction[] getLootFunctions() {
        
    //     return functions;
    // }

    @Override
    public int getWeight() {
        
        return this.weight;
    }

    @Override
    public int getquality() {
        
        return this.quality;
    }


}