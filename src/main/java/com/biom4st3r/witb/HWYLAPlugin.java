package com.biom4st3r.witb;

import java.util.List;

import com.biom4st3r.whatsinthebox.PacketInit;
import com.biom4st3r.whatsinthebox.WhatsInTheBox;
import com.biom4st3r.whatsinthebox.api.AccessableLootTable;
import com.biom4st3r.whatsinthebox.api.LootTableStorage;
import com.biom4st3r.whatsinthebox.mxn.LootPoolMxn;
import com.biom4st3r.witb.interfaces.ChestBEAccess;
import com.biom4st3r.witb.interfaces.ItemEntryAccess;

import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import mcp.mobius.waila.api.IRegistrar;
import mcp.mobius.waila.api.IWailaPlugin;
import mcp.mobius.waila.api.TooltipPosition;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.entry.LootEntry;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class HWYLAPlugin implements IWailaPlugin {

    IComponentProvider provider = new IComponentProvider() {
        @Override
        public void appendBody(List<Text> tooltip, IDataAccessor accessor, IPluginConfig config) {
            System.out.println("body");
        }

        @Override
        public void appendTail(List<Text> tooltip, IDataAccessor accessor, IPluginConfig config) {
            System.out.println("tail");
        }

        int counter;

        @Override
        public void appendHead(List<Text> tooltip, IDataAccessor accessor, IPluginConfig config) {
            // ((ITaggableList<Identifier, Text>) tooltip).setTag(new
            // Identifier(Waila.MODID,"object_name"),new LiteralText("Bitch"));

            if (accessor.getBlockEntity() instanceof ChestBlockEntity) {
                ChestBlockEntity chestBE = (ChestBlockEntity) accessor.getBlockEntity();
                // System.out.println(chestBE.toTag(new CompoundTag()));
                Identifier id = ((ChestBEAccess) chestBE).getLootTableID();
                // System.out.println(id);
                // System.out.println(LootTableStorage.instance().get(id));
                // System.out.println(LootTableStorage.instance().containsKey(id));

                if (id == null)
                    return;
                MinecraftClient.getInstance().getNetworkHandler()
                        .sendPacket(PacketInit.CLIENT.createRequestLootTable(id));
                if (LootTableStorage.instance().get(id) != null) {
                    AccessableLootTable table = LootTableStorage.instance().getAccessableLootTable(id);
                    LootPoolMxn[] pools = table.getAccessableLootPools();
                    if(pools.length > 0)
                    {
                        LootEntry[] loots = pools[0].getEntries();
                        int sum = 0;
                        if(!(loots[0] instanceof ItemEntry))
                        {
                            System.out.println("not ItemEntry");
                            return;
                        }
                        for (int i = 0; i < loots.length; i++) {
                            ItemEntryAccess ie = (ItemEntryAccess) loots[i];
                            sum += ie.getWeight();
                        }
                        for (int i = 0; i < loots.length; i++) {
                            ItemEntryAccess ie =  ((ItemEntryAccess)loots[i]);
                            tooltip.add(new LiteralText(String.format("%s %.1f%%",new TranslatableText(ie.getItem().getTranslationKey()).asFormattedString(),((float)ie.getWeight())/((float)sum)*100f)));
                        }
                    }
                }
                else if(!LootTableStorage.instance().containsKey(id))
                {
                    WhatsInTheBox.requestLootTable(id);
                    LootTableStorage.instance().put(id, null);
                }
                else
                {
                    ++counter;
                    String loading = "Loading";
                    if(counter%60 < 19)
                    {
                        loading+=".";
                    }
                    else if(counter%60 < 39)
                    {
                        loading+="..";
                    }
                    else
                    {
                        loading+="...";
                    }
                    tooltip.add(new LiteralText(loading));
                }
            }
        }
    
    };

    @Override
    public void register(IRegistrar registrar) {
        registrar.registerComponentProvider(provider,TooltipPosition.HEAD,ChestBlockEntity.class);

    }
}