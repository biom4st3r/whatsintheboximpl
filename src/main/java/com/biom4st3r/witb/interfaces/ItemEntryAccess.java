package com.biom4st3r.witb.interfaces;

import net.minecraft.item.Item;

/**
 * EntryAccess
 */
public interface ItemEntryAccess {

    public Item getItem();
    public int getWeight();
    public int getquality();

}