package com.biom4st3r.witb.interfaces;

import net.minecraft.util.Identifier;

public interface ChestBEAccess
{
    public Identifier getLootTableID();
}